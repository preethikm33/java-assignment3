package one;
import java.util.HashSet;

public class Hash {
	public static void main(String args[]){  
		  HashSet<String> set=new HashSet<String>();  
		           set.add("Chemistry");  
		           set.add("Mathematics");  
		           set.add("Biology");  
		           set.add("English");  
		           System.out.println("The first set of textbooks contained in first stack are: "+set);  
		           
		  HashSet<String> set1=new HashSet<String>();  
		           set1.add("Biology");  
		           set1.add("English"); 
		           set1.add("Geography"); 
		           set1.add("Physics"); 
		           System.out.println("The second set of textbooks contained in second stack are: "+set1);  
		
		set.retainAll(set1);  
		System.out.println("Present in both stack: "+ set);   
                    
	}
}

